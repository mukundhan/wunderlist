var mainLists = [],
    mainTasks = [],
    subTasks = [],
    currentListName = "",
    currentListId = "",
    currentMainTaskName = "",
    currentMainTaskId = "";
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var today = new Date();
document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        document.getElementById("main-task-add").placeholder = "<-Select an Main List To add Notes";
        document.getElementsByClassName("add-main-task-input")[0].placeholder = "Add New List";
        //bindEventOnClick("inbox", displayMainTasks);
        bindEventOnClick("today", displayMainTasks);
        bindEventOnKeyEnter("add-main-task-input", addNewMainList)
        bindEventOnKeyEnter("main-task-add", createMainTask);
        mainLists.push({
            name: "inbox",
            id: generateTimeStampId(),
            mainTasks: []
        });
        mainLists.push({
            name: "today",
            id: generateTimeStampId(),
            mainTasks: []
        });
        readTextFile("/json/mainList.json", function(text) {
            var data = JSON.parse(text);
            console.log(data);
            if (data) {
                mainLists = data.mainLists;
                populateMainLists(mainLists);
            }
        });
    }
};

function saveLocally() {
    var mainListsJsonAsString = JSON.stringify({ mainLists: mainLists });
    saveJsonObject(mainListsJsonAsString, 'mainList.json', 'text/json');
}

function saveJsonObject(text, name, type) {
    var a = document.createElement("a");
    var file = new Blob([text], { type: type });
    a.href = URL.createObjectURL(file);
    a.download = name;
    a.click();
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

function getChildrenById(element, id) {
    var children = element.childNodes;
    var result;
    children.forEach(function(item, index, array) {
        if (item.getAttribute("id") == id) {
            result = children[index];
            break;
        }
    });
    return result;
}

function getChildrenByClassName(element, className) {
    var children = element.childNodes;
    var result;
    children.forEach(function(item, index, array) {
        if (item.className == className) {
            result = children[index];
            break;
        }
    });
    return result;
}

function getFirstChildrenByElement(element) {
    return element.firstChild;
}

function getLastChildrenByElement(element) {
    return element.lastChild;
}

function generateTimeStampId() {
    console.log("timeStamp", new Date().valueOf());
    return new Date().valueOf();
}

function getElementById(id) {
    return document.getElementById(id);
}

function hideElementById(id) {
    document.getElementById.classList.add("hidden");
    //    finished-task-add
}

function createElement(JSONObject) {
    var element;
    if (JSONObject.element) {
        element = document.createElement(JSONObject.element);
    }
    if (JSONObject.class) {
        element.className = JSONObject.class;
    }
    if (JSONObject.type) {
        element.type = JSONObject.type;
    }
    if (JSONObject.name) {
        element.name = JSONObject.name;
    }
    if (JSONObject.value) {
        element.value = JSONObject.value;
    }
    if (JSONObject.id) {
        element.id = JSONObject.id;
    }
    if (JSONObject.textContent) {
        element.textContent = JSONObject.textContent;
    }
    if (JSONObject.name) {
        element.setAttribute("name", JSONObject.name);
    }
    if (JSONObject.placeholder) {
        element.placeholder = JSONObject.placeholder;
    }
    if (JSONObject.readOnly) {
        element.readOnly = JSONObject.readOnly;
    }
    if (JSONObject.ariaHidden) {
        element.setAttribute("aria-hidden", JSONObject.ariaHidden);
    }
    return element;
}

function bindEventOnClick(id, functionName) {
    document.getElementById(id).addEventListener('click', function() { functionName(id) });
}

function getByClassName(className) {
    return document.getElementsByClassName(className)[0];
}

function emptyInputById(id) {
    document.getElementById(id).value = '';
}

function emptyElementById(id) {
    document.getElementById(id).innerHTML = '';
}

function deleteElementById(id) {
    if (document.getElementById(id)) {
        document.getElementById(id).parentNode.removeChild(document.getElementById(id));
    }
}

function bindEventOnKeyEnter(id, functionName) {
    document.getElementById(id).addEventListener('keydown', function() {
        if (event.keyCode == 13) {
            functionName();
        }
    });
}

function populateMainLists(mainLists) {
    listLength = mainLists.length;
    var counter;
    for (counter = 0; counter < listLength; counter++) {
        if (counter > 1) {
            var tempList = mainLists[counter];
            appendListToView(tempList.id, tempList.name);
        }
    }
}

function appendListToView(id, mainListName) {
    console.log("mainLists", mainLists);
    var list = createElement({
        element: "li"
    });
    var icon = createElement({
        element: "i",
        class: "fa fa-bars"
    });
    var div = createElement({
        element: "div",
        class: "f-list",
        textContent: mainListName
    });
    list.appendChild(icon);
    list.appendChild(div);
    list.addEventListener("click", function() { displayMainTasks(mainListName, id); });
    document.getElementById("main-ul").appendChild(list);
    emptyInputById("add-main-task-input");
};

function addNewMainList() {
    var mainListName = document.getElementById("add-main-task-input").value;
    if (mainListName) {
        var mainList = {
            name: "",
            mainTasks: []
        };
        mainList.name = "";
        var listLength = mainLists.length;
        var counter;
        if (listLength != 0) {
            for (counter = 0; counter < listLength; counter++) {
                if (mainLists[counter].name == mainListName) {
                    toast("Task with Name " + mainListName + " Already Exists!");
                } else {
                    if (counter == listLength - 1) {
                        mainList.name = mainListName;
                        mainList.id = generateTimeStampId();
                        mainLists.push(mainList);
                        appendListToView(mainList.id, mainList.name);
                    }
                }
            }
        } else {
            mainList.name = mainListName;
            mainList.id = generateTimeStampId();
            mainLists.push(mainList);
            appendListToView(mainList.id, mainList.name);
        }
    }
}

function createMainTask() {
    var mainTask = document.getElementById("main-task-add").value;
    if (mainTask) {
        addNewMainTask(mainTask, addNewMainTask);
        emptyInputById("main-task-add");
    }
}

function addNewMainTask(taskName, callback) {
    var mainTask = {
        name: "",
        subtasks: [],
        createdOn: new Date(),
        completed: false,
        isImportant: false
    };
    var tasksLength = mainLists.length;
    var counter;
    if (tasksLength != 0) {
        for (counter = 0; counter < tasksLength; counter++) {
            if (mainLists[counter].name == currentListName) {
                addToMainList(counter, taskName, appendMainTaskToView);
            } else {
                if (counter == tasksLength - 1) {
                    toast("No List Found .. Please Select a List and Add Task in it..");
                }
            }
        }
    } else {
        toast("Select a List To add Tasks in it.");
    }

    function addToMainList(index, taskName, callback) {
        var taskLength = mainLists[index].mainTasks.length;
        var mainTasks = mainLists[index].mainTasks;
        if (taskLength != 0) {
            for (counter = 0; counter < taskLength; counter++) {
                if (mainTasks[counter].name == taskName) {
                    toast("Task With Name already Exists..Please Add a new task.");
                } else {
                    if (counter == taskLength - 1) {
                        mainTask.name = taskName;
                        mainTask.id = generateTimeStampId();
                        mainLists[index].mainTasks.push(mainTask);
                        console.log("mainlists After Adding task ", mainLists);
                        callback(mainTask);
                    }
                }
            }
        } else {
            mainTask.name = taskName;
            mainTask.id = generateTimeStampId();
            mainLists[index].mainTasks.push(mainTask);
            console.log("mainlists After Adding task ", mainLists);
            callback(mainTask);
        }
    }
}


function displayMainTasks(listName, id) {
    deleteElementById("taskAddDiv");
    currentListName = listName;
    currentListId = id;
    document.getElementById("main-task-add").placeholder = "Add a Task in ' " + listName + " '... ";
    emptyElementById("main-task-list");
    emptyElementById("finished-task-list");
    var listLength = mainLists.length;
    var counter;
    if (listLength != 0) {
        for (counter = 0; counter < listLength; counter++) {
            if (mainLists[counter].id === id) {
                if (mainLists[counter].mainTasks) {
                    populateMainTasks(mainLists[counter].mainTasks);
                }
            } else {
                if (counter == listLength - 1) {
                    toast("No tasks have Been Created Yet for List " + listName + "!");
                }
            }
        }
    } else {
        toast("No List Found With This Name , Create a New List with " + listName + "!");
    }
}

function populateMainTasks(mainTasks) {
    var taskLength = mainTasks.length;
    if (taskLength != 0) {
        for (counter = 0; counter < taskLength; counter++) {
            (function() { appendMainTaskToView(mainTasks[counter]); }());
        }
    }
}

/** creates the task as the user adds and appends in the task note pad */
function createSubTask() {
    var subTask = {
        name: ""
    };
    var todo = document.getElementById("subtask").value;
    subTask.name = todo;
    subTask.id = generateTimeStampId();
    console.log("subTask", subTask);
    addToSubtasksList(subTask, appendSubTasksToNotepad);

    /**append the text to the note padd one by one as the user adds the text*/
    function appendSubTasksToNotepad(subTasks) {
        (function() {
            console.log("populatingSubTask", subTasks);
            populateSubTasks(subTasks, currentMainTaskName);
            emptyInputById("subtask");
        }());
    }
};

function addToSubtasksList(task, callback) {
    subtasks = [];
    mainLists.some(function(item) {
        if (item.id === currentListId) {
            item.mainTasks.some(function(list) {
                if (list.id === currentMainTaskId) {
                    list.subtasks.push(task);
                    subtasks = list.subtasks;
                    callback(subtasks);
                }
            })
        }
    });
    console.log("after adding subtasks ", mainLists);

};

function populateSubTasks(subTasks, mainTaskName) {
    emptyElementById("paper-clip");
    if (subTasks) {
        subTasks.some(function(item) {
            var li = createElement({
                element: "li",
                id: item.id,
                textContent: item.name
            });
            document.getElementById("paper-clip").appendChild(li);
        });
    } else {
        toast("No Sub Tasks have been Added to to " + mainTaskName);
    }
}

function deleteMainTask() {
    console.log("deleteMainTask with ", currentListName + " " + currentMainTaskName);
    if (confirm('Are you sure you want to delete MainTask ' + currentMainTaskName + '?')) {
        mainLists.some(function(item) {
            if (item.id === currentListId) {
                item.mainTasks.some(function(list) {
                    if (list.id === currentMainTaskId) {
                        var index = item.mainTasks.indexOf(list);
                        item.mainTasks.splice(index, 1);
                        deleteElementById("taskAddDiv");
                        displayMainTasks(currentListName, currentListId);
                        toast("Main Task With Name " + currentMainTaskName + " deleted!");
                    }
                });
            }
        });
    }
}

function changeMainTaskStatus(JsonObject) {
    mainLists.some(function(item) {
        if (item.id === currentListId) {
            item.mainTasks.some(function(list) {
                if (list.id === currentMainTaskId) {
                    if (JsonObject.render) {

                    } else if (JsonObject.completed != null) {
                        list.completed = JsonObject.completed;
                    } else if (JsonObject.isImportant != null) {
                        list.isImportant = !list.isImportant;
                    }
                    console.log("after Completed ", mainLists);
                }
            });
            emptyElementById("main-task-list");
            emptyElementById("finished-task-list");
            populateMainTasks(item.mainTasks);
        }
    });
}

function getMainTaskById(id) {
    var mainTask;
    mainLists.some(function(item) {
        if (item.id === currentListId) {
            item.mainTasks.some(function(list) {
                if (list.id === id) {
                    mainTask = list;
                    break;
                }
            });
        }
    });
    return mainTask;
}

function applyMainTaskChanges(mainTaskName, id) {
    appendSubTaskMenuToView(mainTaskName, id);
    var headTextDiv = document.getElementById("noteHeadText");
    console.log("event", event.target);
    listLength = mainLists.length;
    currentMainTaskName = mainTaskName;
    currentMainTaskId = id;
    console.log("currentMainTaskName", currentMainTaskName);
    console.log("currentMainTaskId", currentMainTaskId);
    if (event.target.tagName == "DIV") {
        var mainTask = getMainTaskById(id);
        setTimeout(function() {
            if (headTextDiv) {
                if (mainTask.completed) {
                    if (!document.getElementById("big-strike-through")) {
                        var bigStrikeDiv = createElement({
                            element: "div",
                            class: "big-strike",
                            id: "big-strike-through"
                        });
                        document.getElementById("note-main-div")
                            .insertBefore(bigStrikeDiv, document.getElementById("note-main-div").childNodes[0]);
                        document.getElementById("big-chkbox").checked = false;
                    }
                } else {
                    if (document.getElementById("big-strike-through")) {
                        deleteElementById("big-strike-through");
                        document.getElementById("big-chkbox").checked = true;
                    }
                }
                if (mainTask.isImportant) {
                    if (!document.getElementById("big-star").classList.contains("bg-red")) {
                        document.getElementById("big-star").classList.add("bg-red");
                    }
                } else {
                    if (document.getElementById("big-star").classList.contains("bg-red")) {
                        document.getElementById("big-star").classList.remove("bg-red");
                    }
                }
            }
        }, 100)
    }
    if (event.target.type == "checkbox") {
        var isMainTaskCompleted = event.target.checked;
        console.log("isChecked", isMainTaskCompleted);
        changeMainTaskStatus({
            completed: isMainTaskCompleted
        });
    } else if (event.target.tagName == "I") {
        changeMainTaskStatus({
            isImportant: true
        });
    } else {
        changeMainTaskStatus({
            render: true
        });
    }
}


function appendSubTaskMenuToView(mainTaskName, id) {
    listLength = mainLists.length;
    currentMainTaskName = mainTaskName;
    console.log("currentMainTaskName", currentMainTaskName);
    console.log("currentMainTaskId", currentMainTaskId);
    var task = document.getElementById("taskAddDiv");
    if (!task) {
        var taskAddDiv = createElement({
            element: "div",
            class: "task-add-div",
            id: "taskAddDiv"
        });
        var taskAddContent = createElement({
            element: "div",
            class: "task-add-content"
        });

        var noteAppHead = createElement({
            element: "div",
            class: "note-app-head",
            id: "noteHeadText"
        });
        var checkBoxDiv = createElement({
            element: "div",
            class: "note-checkbox",
            id: id
        });
        var noteCheckBox = createElement({
            element: "input",
            class: "big-chkbox",
            type: "checkbox",
            name: mainTaskName,
            id: "big-chkbox",
            value: id
        });
        checkBoxDiv.appendChild(noteCheckBox);
        noteAppHead.appendChild(checkBoxDiv);
        var noteHeadText = createElement({
            element: "div",
            class: "note-mak"
        });
        var noteTextSpan = createElement({
            element: "div",
            class: "note-text",
            id: "noteText",
            textContent: mainTaskName
        });
        var starIcon = createElement({
            element: "i",
            class: "fa fa-star fa-star-1",
            id: "big-star"
        });
        noteHeadText.appendChild(noteTextSpan);
        noteHeadText.appendChild(starIcon);
        noteAppHead.appendChild(noteHeadText);


        var noteContentDiv = createElement({
            element: "div",
            class: "nav-bar-2"
        });

        var noteContentList = createElement({
            element: "ul"
        });
        var eventDateList = createElement({
            element: "li",
            class: "li-2 cl-blue"
        });
        var calenderIcon = createElement({
            element: "i",
            class: "fa fa-calendar"
        });
        var eventDate = createElement({
            element: "div",
            class: "f-list-2",
            textContent: monthNames[today.getMonth()] + "-" + today.getDate() + "-" +
                days[today.getDay()]
        });
        eventDateList.appendChild(calenderIcon);
        eventDateList.appendChild(eventDate);
        var eventRemindMeList = createElement({
            element: "li",
            class: "li-2 opac-5"
        });
        var clockIcon = createElement({
            element: "i",
            class: "fa fa-clock-o"
        });
        var remindMeDiv = createElement({
            element: "div",
            class: "f-list-2",
            textContent: "Remind at " + today.getHours() + "-" + today.getMinutes(),
        });
        eventRemindMeList.appendChild(clockIcon);
        eventRemindMeList.appendChild(remindMeDiv);

        noteContentList.appendChild(eventDateList);
        noteContentList.appendChild(eventRemindMeList);
        noteContentDiv.appendChild(noteContentList);
        var subTaskAddDiv = createElement({
            element: "div",
            class: "add-sub-div"
        });
        var divElement = createElement({
            element: "div"
        });
        var addSubTaskDiv = createElement({
            element: "div",
            class: "add-sub-plus-div",
            id: "addSubTaskDiv"
        });
        addSubTaskDiv.addEventListener('click', function() { createSubTask(); });
        var addIcon = createElement({
            element: "i",
            class: "fa fa-plus"
        });
        addSubTaskDiv.appendChild(addIcon);
        var addSubtaskInput = createElement({
            element: "input",
            class: "subtask-text-input",
            id: "subtask",
            type: "text",
            placeholder: "Add a subtask"
        });
        divElement.appendChild(addSubTaskDiv);
        divElement.appendChild(addSubtaskInput);
        subTaskAddDiv.appendChild(divElement);

        var paperClipContent = createElement({
            element: "div"
        });
        var paperClipDiv = createElement({
            element: "div",
            class: "paper-clip",
            readOnly: true
        });
        var subTasksList = createElement({
            element: "ol",
            id: "paper-clip"
        });
        paperClipDiv.appendChild(subTasksList);
        paperClipContent.appendChild(paperClipDiv);
        var actionsDiv = createElement({
            element: "div",
            class: "task-footer"
        });
        var trashIcon = createElement({
            element: "i",
            class: "fa fa-trash"
        })
        trashIcon.addEventListener('click', function() { deleteMainTask(); });
        var playIcon = createElement({
            element: "i",
            class: "fa fa-play"
        })
        actionsDiv.appendChild(trashIcon);
        actionsDiv.appendChild(playIcon);
        taskAddContent.appendChild(noteAppHead);
        taskAddContent.appendChild(noteContentDiv);
        taskAddContent.appendChild(subTaskAddDiv);
        taskAddContent.appendChild(paperClipContent);
        taskAddContent.appendChild(actionsDiv);
        taskAddDiv.appendChild(taskAddContent);
        console.log(taskAddDiv);
        document.getElementById('subTaskContent').appendChild(taskAddDiv);
    } else {
        document.getElementById("noteText").textContent = mainTaskName;
    }
    setTimeout(function() {
        appendSubTaskChangesToView()
    }, 100);
}

function appendSubTaskChangesToView() {
    mainLists.some(function(item) {
        if (item.id === currentListId) {
            item.mainTasks.some(function(list) {
                if (list.id === currentMainTaskId) {
                    populateSubTasks(list.subtasks, currentMainTaskName);
                }
            });
        }
    });
}

function appendMainTaskToView(mainTask) {
    var finishedTaskList = document.getElementById("finished-task-list");
    var incompletedTaskList = document.getElementById("main-task-list");
    var headTextDiv = document.getElementById("noteHeadText");
    var important = "";
    if (mainTask.isImportant) {
        important = "bg-red";
    }
    var mainDiv = createElement({
        element: "div",
        class: "note-item-btn ",
        id: "note-main-div"
    });
    mainDiv.addEventListener('click', function() { applyMainTaskChanges(mainTask.name, mainTask.id); });
    var subDivOne = createElement({
        element: "div",
        class: "note-checkbox "
    });
    var checkbox = createElement({
        element: "input",
        class: "note-checkbox",
        type: "checkbox",
        name: "name",
        value: "value",
        class: "mdm-chkbox",
        id: mainTask.id
    });
    if (mainTask.completed) {
        checkbox.checked = true;
    }
    subDivOne.appendChild(checkbox);
    mainDiv.appendChild(subDivOne);
    //sub-div-2
    var subDivTwo = createElement({
        element: "div",
        class: "note-mak"
    });
    var noteSpan = createElement({
        element: "span",
        textContent: mainTask.name
    });
    subDivTwo.appendChild(noteSpan);
    mainDiv.appendChild(subDivTwo);
    //sub-div-3
    var subDivThree = createElement({
        element: "div",
        class: "thumb-pin"
    });
    var iconSpan = createElement({
        element: "span"
    });
    var thumbIcon = createElement({
        element: "i",
        class: "fa fa-thub-tack"
    });
    iconSpan.appendChild(thumbIcon);
    subDivThree.appendChild(iconSpan);
    var dateSpan = createElement({
        element: "span",
        textContent: monthNames[today.getMonth()] + "-" + today.getDate(),
        //textContent: "today"
        class: "fl-right"

    });
    var starIcon = createElement({
        element: "i",
        class: "fa fa-star p-l-20 " + important
    });
    dateSpan.appendChild(starIcon);
    subDivThree.appendChild(dateSpan);
    mainDiv.appendChild(subDivThree);
    if (mainTask.completed) {
        var strikeDiv = createElement({
            element: "div",
            class: "strike "
        });
        var bigStrikeDiv = createElement({
            element: "div",
            class: "big-strike",
            id: "big-strike-through"
        });
        mainDiv.insertBefore(strikeDiv, mainDiv.childNodes[0]);
        finishedTaskList.appendChild(mainDiv);
        setTimeout(function() {
            if (headTextDiv) {
                if (!document.getElementById("big-strike-through")) {
                    headTextDiv.insertBefore(bigStrikeDiv, headTextDiv.childNodes[0]);
                    document.getElementById("big-chkbox").checked = true;
                }
                if (mainTask.isImportant) {
                    document.getElementById("big-star").classList.add("bg-red");
                } else {
                    document.getElementById("big-star").classList.remove("bg-red");
                }
            }
        }, 100)
    } else {
        incompletedTaskList.appendChild(mainDiv);
        deleteElementById("big-strike-through");
        setTimeout(function() {
            if (headTextDiv) {
                if (!document.getElementById("big-strike-through")) {
                    document.getElementById("big-chkbox").checked = false;
                    if (mainTask.isImportant) {
                        document.getElementById("big-star").classList.add("bg-red");
                    } else {
                        document.getElementById("big-star").classList.remove("bg-red");
                    }
                }
            }
        }, 100)
    }
}

function toast(message) {
    var toaster = document.getElementById("toaster");
    console.log("toaster", toaster)
    if (!toaster) {
        var toasterDiv = createElement({
            element: "div",
            class: "toaster-div",
            id: "toaster"
        });
        var toasterMessageDiv = createElement({
            element: "div",
            class: "toast-message"
        });
        var toastLoader = createElement({
            element: "span",
            class: "toast-loader",
            id: "toaster-loader"
        });
        var toastClose = createElement({
            element: "span",
            class: "toaster-close",
            textContent: "x"
        });
        var toasterHeading = createElement({
            element: "h2",
            textContent: "Message"
        });
        var toastMessage = createElement({
            element: "span",
            textContent: message
        });
        toasterMessageDiv.appendChild(toastLoader);
        toasterMessageDiv.appendChild(toastClose);
        toasterMessageDiv.appendChild(toasterHeading);
        toasterMessageDiv.appendChild(toastMessage);
        toasterDiv.appendChild(toasterMessageDiv);
        document.getElementById("toasterDiv").appendChild(toasterDiv);
        (function() {
            setTimeout(function() {
                document.getElementById("toaster-loader").classList.add("loaded");
            }, 100);
            setTimeout(function() {
                var tempElement = document.getElementById("toaster");
                tempElement.classList.add("fade");
                setTimeout(function() {
                    document.getElementById("toaster").parentNode.innerHTML = "";
                }, 1000);
            }, 2500);
        }());
    } else {
        document.getElementById("toasterDiv").innerHTML = "";
        toast(message);
    }
}
class Task {
    constructor(id, details) {
        this.id = id;
        this.details = details;
        this.createdOn = new Date();
    }
    showDetailsAndCreatedOn() {
        console.log("Task name " + name + " and createdOn " + createdOn);
    }
};